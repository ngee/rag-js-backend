const dotenv = require("dotenv");
dotenv.config();

const express = require("express");
const bodyParse = require("body-parser");
const llamaindex = require("llamaindex");
const cors = require("cors");



const app = express();
app.use(bodyParse.json());
app.use(cors());

let queryEngine1;
let queryEngine2;
let queryEngine;


// load the pdf documents
new llamaindex.SimpleDirectoryReader()
  .loadData({ directoryPath: "./data" })
  .then((documents) => {
    llamaindex.VectorStoreIndex.fromDocuments(documents).then(async (index) => {
      queryEngine = await index.asQueryEngine();
    });
  });

new llamaindex.SimpleDirectoryReader()
  .loadData({ directoryPath: "./data2" })
  .then((documents) => {
    llamaindex.VectorStoreIndex.fromDocuments(documents).then(async (index) => {
      queryEngine2 = await index.asQueryEngine();
    });
  });


// create query and function tools
function sumTwoNumbers({ a, b }) {
  return a + b;
}

const sumJSON = {
  type: "object",
  properties: {
    a: {
      type: "number",
      description: "The first number",
    },
    b: {
      type: "number",
      description: "The second number",
    },
  },
  required: ["a", "b"],
};

const routerQueryEngine = new llamaindex.RouterQueryEngine({
  queryEngineTools: [
    {
      queryEngine: queryEngine1,
      name: "physical_standards_letters",
      description:
        "Useful for Physical Standards for Letters, Cards, Flats, and Parcels",
    },
    {
      queryEngine: queryEngine2,
      name: "midnight_library",
      description: "Useful for questions about midnight library the book",
    },
  ],
  verbose: true,
});

const sumFunctionTool = new llamaindex.FunctionTool(sumTwoNumbers, {
  name: "sumNumbers",
  description: "Use this function to sum two numbers",
  parameters: sumJSON,
});

const queryEngineTool = new llamaindex.QueryEngineTool({
  queryEngine: routerQueryEngine,
  metadata: {
    name: "midnight_library_physical_standards_letters",
    description:
      "A tool that can answer questions about Midnight Libray and Pysical standards for letters",
  },
});

const agent = new llamaindex.OpenAIAgent({
  tools: [queryEngineTool, sumFunctionTool],
  verbose: true,
});



// routes
app.get("/", (_req, res) => {
  return res.status(200).send({
    message: "Welcome to the server!",
  });
});

app.post("/chat", async (req, res) => {
  const data = req.body;
  await agent.chat({ message: data.query }).then((answer) => {
    return res.status(200).send(answer.toString());
  });
});

app.post("/query", async(req, res) => {
  queryEngine.query({ query: data.query }).then((answer) => {
    return res.status(200).send(answer.toString());
  });
})

app.listen(8080, () => {
  console.log(`app is listening on port 8080`);
});
