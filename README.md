# RAG with llamaindex

## Tools
VueJS, Node, llamaindex

## Getting started

- create a `.env` file, copy the content from `.env.sample` and replace with your api key
- cd to backend folder and run `npm install` followed by `npm start`
- cd to frontend folder and run `npm install` followed by `npm start`

Your frontend and backend should be running now `http://localhost:5173/` and `http://localhost:8080/` respectively


## Endpoints

| Method | Route    | Description          |
| ------ | -------- | -------------------- |
| GET    | /        | Welcome page         |
| POST   | /chat    | Send chat message    |
| POST   | /query   | Submit a query       |

